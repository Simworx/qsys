The following steps need to be performed to import the project file into TwinCAT XAE:

1. Start Visual Studio 
2. File --> open--> Project/Solution --> select .tszip file in this folder

Steps to install Tc3_Qrc.compiled-library:

1. Open the Library repository --> Install --> select the compiled-library in this folder.
2. Build Configuration --> Login --> Run